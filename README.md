Quickly spin up Dockerized WMF Phabricator locally

Fetch and spin up adding some sample data:

`./fresh_install`

After install, you may do the following

Shell to the mysql db:

`./shellto_db`

Print all mysql db schemas:

`./dump_all_schemas`